# choco-postman (postman-alt)

## Chocolatey package for postman

This is a different package than [https://github.com/kendaleiv/chocolatey-postman](https://github.com/kendaleiv/chocolatey-postman).

It isn't a fork, when I searched for 'postman', the packages site didn't yield any results (for some reason...). When I tried searching again, it found the appropriate package.

I'll be maintaining this to be sure it has the latest [Postman](https://getpostman.com) versions. It also borrows from [https://github.com/Thilas/chocolatey-packages/tree/master/atom](https://github.com/Thilas/chocolatey-packages/tree/master/atom) to deal with quotes in the uninstall string.