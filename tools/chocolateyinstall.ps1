﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://dl.pstmn.io/download/latest/win32/'
$url64      = 'https://dl.pstmn.io/download/latest/win64/'

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'exe'
  url           = $url
  url64bit      = $url64

  softwareName  = 'postman*'

  checksum      = '4c5e73732064b935d1d4e40df11d81e481b2beea0369135b3cbaed1c2ebc1075'
  checksumType  = 'sha256'
  checksum64    = '0371af340fa50cabc12a4752bb7290dd67992d7451abb7511d9261fbc509c36a'
  checksumType64= 'sha256'
  silentArgs   = '-s'
}

Install-ChocolateyPackage @packageArgs










    








