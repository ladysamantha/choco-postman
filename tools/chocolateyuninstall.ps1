﻿$ErrorActionPreference = 'Stop';

$packageName   = $env:ChocolateyPackageName
$softwareName  = 'postman*'
$fileType      = 'exe'
$silentArgs   = '--uninstall -s'
$validExitCodes= @(0)

[array]$key = Get-UninstallRegistryKey -SoftwareName $softwareName

if ($key.Count -eq 1) {
  $key | % {
    if ($_.UninstallString) {
      function Split-CommandLine {
        param([string]$file)
        return $file
      }
      $file = Invoke-Expression "Split-CommandLine $($_.UninstallString)"
    }
    Write-Debug -Message "File: $file"
    if ($file -and (Test-Path $file)) {
      Uninstall-ChocolateyPackage -PackageName $packageName `
                                  -FileType $fileType `
                                  -SilentArgs $silentArgs `
                                  -ValidExitCodes $validExitCodes `
                                  -File $file
    } else {
      Write-Warning "$packageName has already been uninstalled by other means. Unknown uninstaller: $file ($($_.UninstallString))."
    }
  }
} elseif ($key.Count -eq 0) {
  Write-Warning "$packageName has already been uninstalled by other means."
} elseif ($key.Count -gt 1) {
  Write-Warning "$($key.Count) matches found!"
  Write-Warning "To prevent accidental data loss, no programs will be uninstalled."
  Write-Warning "Please alert package maintainer the following keys were matched:"
  $key | % {Write-Warning "- $($_.DisplayName)"}
}


